# 2.3.0
GetNested and SetNested now support a removeNestedFieldEscapeSign option. This allows you to escape nested fields, such as `foo.bar.$boo.hoo$`, which will get/set `foo.bar['boo.hoo']`, instead of `foo.bar['$boo.hoo$']`

# 2.2.0
- Added the flattenObject and stringifyNestedObjects methods.
- Updated the docs.

# 2.1.3
- DevDeps update - added immutable there too.

# 2.1.2
- CI fix.

# 2.1.1
- Moved immutable to peerDeps.

# 2.1.0
- Added the CustomError class.

# 2.0.0
- First NPM release.

# 2.0.0-rc.1
- Integrated Instanbul.js/NYC - full test code coverage.

# 2.0.0-rc.0
- Initial version. Contains the `arraySort`, `changeKeyCase`, `checkRoutes`, `decodeQueryValues`, `emptyToNull`, `findVertexInGraph` (formerly `findVertexByIdDFS`), `getNested` and `setNested` functionality from ramster@1.x's toolbelt module, rewritten in TypeScript, improved and exported into a separate module. Please check the  docs in the readme.md file for usage, as all of the methods have breaking changes.
- This is the first release candidate, not the proper v2 itself. It doesn't contain all badges in the readme.md file and it's not yet configured in terms of ci, code coverage, etc.
