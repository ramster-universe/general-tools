import {decodeQueryValues} from '../../dist'
import {strict as assert} from 'assert'

describe('decodeQueryValues', function() {
	it('should execute successfully and return undefined if no input argument is provided', function() {
		let result = decodeQueryValues(undefined)
		assert.strictEqual(result, null, `bad value ${result} for the method execution result, expected null`)
	})
	it('should execute successfully and return null if a null input argument is provided', function() {
		let result = decodeQueryValues(null)
		assert.strictEqual(result, null, `bad value ${result} for the method execution result, expected null`)
	})
	it('should execute successfully and return the decoded string if a string input argument is provided and no options are provided', function() {
		let input = '?test=true&test2=false&test3=10&test4=abc',
			result = decodeQueryValues(encodeURIComponent(input))
		assert.strictEqual(result, input, `bad value ${result} for the method execution result, expected ${input}`)
	})
	it('should execute successfully and return the decoded string if a string input argument is provided and the castTrueAndFalseToBoolean option is set to true, but the input does not cast to a boolean', function() {
		let input = '?test=true&test2=false&test3=10&test4=abc',
			result = decodeQueryValues(encodeURIComponent(input), {castTrueAndFalseToBoolean: true})
		assert.strictEqual(result, input, `bad value ${result} for the method execution result, expected ${input}`)
	})
	it('should execute successfully and return true if a string input argument equal to "true" is provided, the castTrueAndFalseToBoolean option is set to true and the input does cast to a boolean', function() {
		let result = decodeQueryValues('true', {castTrueAndFalseToBoolean: true})
		assert.strictEqual(result, true, `bad value ${result} for the method execution result, expected ${true}`)
	})
	it('should execute successfully and return false if a string input argument equal to "false" is provided, the castTrueAndFalseToBoolean option is set to true and the input does cast to a boolean', function() {
		let result = decodeQueryValues('false', {castTrueAndFalseToBoolean: true})
		assert.strictEqual(result, false, `bad value ${result} for the method execution result, expected ${false}`)
	})
	it('should execute successfully and return the decoded string if a string input argument is provided and a parseNumbersToFloat option is set to true, but the input does not parse to a float value correctly', function() {
		let input = '?test=true&test2=false&test3=10&test4=abc',
			result = decodeQueryValues(encodeURIComponent(input), {parseNumbersToFloat: true})
		assert.strictEqual(result, input, `bad value ${result} for the method execution result, expected ${input}`)
	})
	it('should execute successfully and return the input, parsed to a number, if a string input argument that parses to a number correctly is provided and the parseNumbersToFloat option is set to true', function() {
		let result = decodeQueryValues('1.5', {parseNumbersToFloat: true})
		assert.strictEqual(result, 1.5, `bad value ${result} for the method execution result, expected ${1.5}`)
	})
	it('should execute successfully and decode the provided object in depth if no options are provided', function() {
		let inputObject: {[key: string]: any} = {
				undefinedKey: undefined,
				nullKey: null,
				nonEncodedStringkey: '?test=true&test2=false&test3=10&test4=abc',
				encodedStringKey: encodeURIComponent('?test=true&test2=false&test3=10&test4=abc'),
				booleanTrueKey: true,
				stringTrueKey: 'true',
				booleanFalseKey: false,
				stringFalseKey: 'false',
				dateKey: new Date(),
				testJSONKey: {},
				testJSONKey2: [],
				testJSONKey3: false,
				testJSONArrayKey: [],
				testJSONArrayKey2: 'test',
				_json_testJSONKey: `{"key1": "value1", "key2": "35", "key3": "false"}`,
				_json_testJSONKey2: `{"key1": "value1", "key2": "35", "key3": "false"}`,
				_json_testJSONKey3: `true`,
				_json_testJSONArrayKey: `[{"key1": "value1"}, {"key2": "35"}, {"key3": "false"}]`,
				_json_testJSONArrayKey2: `[{"key1": "value1"}, {"key2": "35"}, {"key3": "false"}]`,
				_json_testJSONKeyWithoutOriginalValues: `{"key1": "value1"}`
			},
			// testJSONKeyShouldBe = {key1: 'value1', key2: '35', key3: 'false'} as {[fieldName: string]: any},
			// testJSONArrayKeyShouldBe = [testJSONKeyShouldBe],
			arrayObject: any[] = []
		for (const key in inputObject) {
			arrayObject.push(inputObject[key])
		}
		let result = decodeQueryValues(
			Object.assign(
				{},
				inputObject,
				{
					nestedObjectKey: inputObject,
					arrayKey: arrayObject.concat(Object.assign({}, inputObject))
				}
			)
		)
		inputObject.undefinedKey = null
		inputObject.encodedStringKey = decodeURIComponent(inputObject.encodedStringKey)
		inputObject.testJSONKey3 = true
		arrayObject[0] = null
		arrayObject[3] = decodeURIComponent(arrayObject[3])
		// for (const key in testJSONKeyShouldBe) {
		// 	assert.strictEqual(
		// 		result.testJSONKey[key],
		// 		testJSONKeyShouldBe[key],
		// 		`bad value ${result.testJSONKey[key]} for the key "${key}" in the result.testJSONKey object, expected ${testJSONKeyShouldBe[key]}`
		// 	)
		// }
		// for (const key in testJSONKeyShouldBe) {
		// 	assert.strictEqual(
		// 		result.testJSONKey2[0][key],
		// 		testJSONKeyShouldBe[key],
		// 		`bad value ${result.testJSONKey2[0][key]} for the key "${key}" in the result.testJSONKey2[0] object, expected ${testJSONKeyShouldBe[key]}`
		// 	)
		// }
		// testJSONArrayKeyShouldBe.forEach((arrayKeyItem, index) => {
		// 	for (const key in arrayKeyItem) {
		// 		assert.strictEqual(
		// 			result.testJSONArrayKey[index][key],
		// 			arrayKeyItem[key],
		// 			`bad value ${result.testJSONArrayKey[index][key]} for the key "${key}" in the result.testJSONArrayKey[${index}] object, expected ${arrayKeyItem[key]}`
		// 		)
		// 	}
		// })
		// testJSONArrayKeyShouldBe.forEach((arrayKeyItem, index) => {
		// 	for (const key in arrayKeyItem) {
		// 		assert.strictEqual(
		// 			result.testJSONArrayKey2[index][key],
		// 			arrayKeyItem[key],
		// 			`bad value ${result.testJSONArrayKey2[index][key]} for the key "${key}" in the result.testJSONArrayKey2[${index}] object, expected ${arrayKeyItem[key]}`
		// 		)
		// 	}
		// })
		delete inputObject.testJSONKey
		delete inputObject.testJSONKey2
		delete inputObject.testJSONArrayKey
		delete inputObject.testJSONArrayKey2
		delete inputObject.testJSONKeyWithoutOriginalValues
		arrayObject.splice(8, arrayObject.length - 8)
		for (const key in inputObject) {
			assert.strictEqual(
				result[key],
				inputObject[key],
				`bad value ${result[key]} for the key "${key}" in the result object, expected ${inputObject[key]}`
			)
		}
		arrayObject.forEach((item, index) => {
			assert.strictEqual(
				result.arrayKey[index],
				item,
				`bad value ${result.arrayKey[index]} for the key "arrayKey.${index}" in the result object, expected ${item}`
			)
		})
	})
})
