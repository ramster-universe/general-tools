import {IDecodeQueryValuesOptions} from './decodeQueryValues.interfaces'

/**
 * Recursively performs decodeURIComponent on an object or value and returns the decoded object. Additionally, if an object is provided, any key that starts with "_json_" will be parsed using JSON.parse().
 * @param input (required) - The object or value to be decoded.
 * @param options (optional) - Flags for additonal processing to be performed on the input, such as casting strings to booleans and parsing string numbers to float.
 * @returns The decoded object or value.
 */
export function decodeQueryValues(input: any, options?: IDecodeQueryValuesOptions): any {
	if ((typeof input === 'undefined') || (input === 'null')) {
		return null
	}
	if (typeof input === 'string') {
		if (options) {
			if (options.castTrueAndFalseToBoolean) {
				if (input === 'true') {
					return true
				}
				if (input === 'false') {
					return false
				}
			}
			if (options.parseNumbersToFloat) {
				let parsedInput = parseFloat(input)
				if (!isNaN(parsedInput)) {
					return parsedInput
				}
			}
		}
		return decodeURIComponent(input)
	}
	if ((typeof input !== 'object') || (input === null) || (input instanceof Date)) {
		return input
	}
	if (input instanceof Array) {
		let decodedObject: any[] = []
		input.forEach((item) => {
			decodedObject.push(decodeQueryValues(item))
		})
		return decodedObject
	}
	let decodedObject: any = {}
	for (const key in input) {
		let decodedKey = decodeURIComponent(key)
		if (decodedKey.substr(0, 6) === '_json_') {
			let actualKey = decodedKey.substr(6, decodedKey.length),
				decodedValues = decodeQueryValues(JSON.parse(decodeURIComponent(input[key])))
			const nonDecodedValues = input[actualKey]
			if (typeof nonDecodedValues === 'undefined') {
				decodedObject[actualKey] = decodedValues
			}
			else {
				/*
				 * decodedValues - array, nonDecodedValues - array = concat
				 * decodedValues - array, nonDecodedValues - not array = add nonDecodedValues to decodedValues
				 * decodedValues - object, nonDecodedValues - array = add decodedValues to nonDecodedValues
				 * decodedValues - object, nonDecodedValues - non-null object = merge
				 * decodedValues - object, nonDecodedValues - not an object = add nonDecodedValues under the _originalValue key to decodedValues
				 * otherwise - overwrite as fallback
				 */
				if (decodedValues instanceof Array) {
					if (nonDecodedValues instanceof Array) {
						decodedObject[actualKey] = nonDecodedValues.concat(decodedValues)
					}
					else {
						decodedObject[actualKey] = decodedValues.push(nonDecodedValues)
					}
				}
				else if ((typeof decodedValues === 'object') && (decodedValues !== null)) {
					if (nonDecodedValues instanceof Array) {
						decodedObject[actualKey] = nonDecodedValues.push(decodedValues)
					}
					else {
						decodedObject[actualKey] = Object.assign({_originalValue: nonDecodedValues}, decodedValues)
					}
				} else {
					decodedObject[actualKey] = decodedValues
				}
			}
		}
		decodedObject[decodedKey] = decodeQueryValues(input[key])
	}
	return decodedObject
}
