export interface IDecodeQueryValuesOptions {
	castTrueAndFalseToBoolean?: boolean
	parseNumbersToFloat?: boolean
}
