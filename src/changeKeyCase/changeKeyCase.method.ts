import {fromJS} from 'immutable'

/**
 * Changes the case of all keys in an object or string and its children between loweCamelCase and UpperCamelCase, based on the provided map.
 * @param keyMap (required) - The map of keys to be converted, i.e. {statusId: 'StatusId', productId: 'ProductId'} or {StatusId: 'statusId', ProductId: 'productId'}.
 * @param input (required) - The input string or object.
 * @returns The string or object with the changed keys.
 */
export function changeKeyCase(
	keyMap: {[inputKey: string]: string},
	input: string | Object,
): string | Object {
	if (typeof input === 'object') {
		if ((input === null) || (input instanceof Date)) {
			return input
		}
		let dereferencedInput = fromJS(input).toJS() // dereference the object
		if (dereferencedInput instanceof Array) {
			return dereferencedInput.map((item: any) => changeKeyCase(keyMap, item))
		}
		let output: {[key: string]: any} = {}
		for (const inputKey in dereferencedInput) {
			const outputKey = keyMap[inputKey],
				item = dereferencedInput[inputKey]
			output[outputKey] = changeKeyCase(keyMap, item)
		}
		return output
	}
	let str = input
	for (const inputKey in keyMap) {
		const outputKey = keyMap[inputKey]
		str = str.replace(new RegExp(`(\\?${inputKey}=)`, 'g'), `?${outputKey}=`).replace(new RegExp(`(&${inputKey}=)`, 'g'), `&${outputKey}=`)
	}
	return str
}
