import {changeKeyCase} from '../../dist'
import {strict as assert} from 'assert'

describe('changeKeyCase', function() {
	it('should execute successfully and return null if null is provided as input', function() {
		const result = changeKeyCase({}, new Date())
		assert(result instanceof Date, `bad value ${result} for the result, expected an instance of Date`)
	})
	it('should execute successfully if a string is provided as input and converting from lower to upper camel case', function() {
		const result = changeKeyCase(
				{
					testKey: 'TestKey',
					testKey1: 'TestKey1',
					testKey2: 'TestKey2',
					testKey3: 'TestKey3',
					testKey4: 'TestKey4'
				},
				'?testKey=1&testKey2=2&TestKey3=3&testKey4=4'
			),
			resultShouldBe = '?TestKey=1&TestKey2=2&TestKey3=3&TestKey4=4'
		assert.strictEqual(result, resultShouldBe, `bad value ${result} for the result, expected ${resultShouldBe}`)
	})
	it('should execute successfully if a string is provided as input and converting from upper to lower camel case', function() {
		const result = changeKeyCase(
				{
					TestKey: 'testKey',
					TestKey1: 'testKey1',
					TestKey2: 'testKey2',
					TestKey3: 'testKey3',
					TestKey4: 'testKey4'
				},
				'?TestKey=1&TestKey2=2&TestKey3=3&TestKey4=4'
			),
			resultShouldBe = '?testKey=1&testKey2=2&testKey3=3&testKey4=4'
		assert.strictEqual(result, resultShouldBe, `bad value ${result} for the result, expected ${resultShouldBe}`)
	})
	it('should execute successfully if an object is provided as input and converting from lower to upper camel case', function() {
		const date = new Date(), 
			result = changeKeyCase(
				{
					testKey: 'TestKey',
					testKey1: 'TestKey1',
					testKey2: 'TestKey2',
					testKey3: 'TestKey3',
					testKey4: 'TestKey4'
				},
				{
					testKey: [{testKey1: 'test'}, date],
					testKey2: 'test test test',
					testKey3: {testKey4: [null, 'test']}
				}
			) as any,
			resultShouldBe = {
				TestKey: [{TestKey1: 'test'}, date.toString()],
				TestKey2: 'test test test',
				TestKey3: {TestKey4: [null, 'test']}
			} as any
		result.TestKey[1] = result.TestKey[1].toString()
		assert.strictEqual(
			result.TestKey[0].TestKey1,
			resultShouldBe.TestKey[0].TestKey1,
			`bad value ${result.TestKey[0].TestKey1} for result.TestKey[0].TestKey1, expected ${resultShouldBe.TestKey[0].TestKey1}`
		)
		assert.strictEqual(
			result.TestKey[1],
			resultShouldBe.TestKey[1],
			`bad value ${result.TestKey[1]} for result.TestKey[1], expected ${resultShouldBe.TestKey[1]}`
		)
		assert.strictEqual(
			result.TestKey2,
			resultShouldBe.TestKey2,
			`bad value ${result.TestKey2} for result.TestKey2, expected ${resultShouldBe.TestKey2}`
		)
		assert.strictEqual(
			result.TestKey3.TestKey4[0],
			resultShouldBe.TestKey3.TestKey4[0],
			`bad value ${result.TestKey3.TestKey4[0]} for result.TestKey3.TestKey4[0], expected ${resultShouldBe.TestKey3.TestKey4[0]}`
		)
		assert.strictEqual(
			result.TestKey3.TestKey4[1],
			resultShouldBe.TestKey3.TestKey4[1],
			`bad value ${result.TestKey3.TestKey4[1]} for result.TestKey3.TestKey4[1], expected ${resultShouldBe.TestKey3.TestKey4[1]}`
		)
	})
})
