import {checkRoutes} from '../../dist'
import {strict as assert} from 'assert'

describe('checkRoutes', function() {
	it('should execute successfully and return true if the routes array contains "*"', function() {
		let result = checkRoutes('/test', ['*'])
		assert.strictEqual(result, true, `bad value ${result} for the method execution result, expected true`)
	})
	it('should execute successfully and return true if the routes array contains the exact provided route', function() {
		let result = checkRoutes('/test', ['/test'])
		assert.strictEqual(result, true, `bad value ${result} for the method execution result, expected true`)
	})
	it('should execute successfully and return true if the routes array contains the route with a paramaters', function() {
		let result = checkRoutes('/test/testParam', ['/test/:param'])
		assert.strictEqual(result, true, `bad value ${result} for the method execution result, expected true`)
	})
	it('should execute successfully and return false if the routes array does not contain the route and there are no route parameters', function() {
		let result = checkRoutes('/test', ['/otherRoute'])
		assert.strictEqual(result, false, `bad value ${result} for the method execution result, expected false`)
	})
	it('should execute successfully and return false if the routes array does not contain the route and there are route parameters', function() {
		let result = checkRoutes('/test2/param', ['/test/:param'])
		assert.strictEqual(result, false, `bad value ${result} for the method execution result, expected false`)
	})
})
