import {flattenObject} from '../../dist'
import {strict as assert} from 'assert'

describe('flattenObject', function() {
	it('should throw an error with the correct message if the top-most object is an array', function() {
		const message = 'The top-most item cannot be an array.'
		let didThrowAnError = false
		try {
			flattenObject([])
		}
		catch(e) {
			didThrowAnError = true
			assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected ${'CustomError'}`)
			assert.strictEqual(e.message, message, `bad value ${e.message} for e.message, expected ${message}`)
		}
		assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
	})
	it('should execute successfully and flatten the provided deeply nested object', function() {
		const date = new Date(),
			result = flattenObject(
				{
					testKey: [{testKey1: 'test'}, date],
					testKey2: 'test test test',
					testKey3: {testKey4: [null, 'test', {tq: 11}]},
					testKey4: null
				}
			),
			resultShouldBe = [
				{key: 'testKey[][testKey1]', value: 'test'},
				{key: 'testKey[]', value: date.toString()},
				{key: 'testKey2', value: 'test test test'},
				{key: 'testKey3[testKey4][]', value: 'test'},
				{key: 'testKey3[testKey4][][tq]', value: 11}
			]
		resultShouldBe.forEach((item, index) => {
			assert.strictEqual(
				result[index].key,
				item.key,
				`bad value ${result[index].key} for resultItem.key at index ${index}, expected ${item.key}`
			)
			let resultValue = result[index].value instanceof Date ? result[index].value.toString() : result[index].value
			assert.strictEqual(
				resultValue,
				item.value,
				`bad value ${resultValue} for resultItem.value at index ${index} (key ${item.key}), expected ${item.value}`
			)
		})
	})
})
