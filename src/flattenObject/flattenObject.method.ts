import {CustomError} from '../index'

/**
 * Takes a deeply nested object and flattens it into top-level key-value pairs. Particularly suitable for turning nested objects into get params.
 * @param object (required) - The object to be flattened. Must not be an array.
 * @param parentKey (optional) - Internally set by the method when executing it for nested objects or arrays.
 * @returns A an array of objects containing of key-value pairs in the format {key, value}.
 */
export function flattenObject(object: {[fieldName: string]: any} | any[], parentKey?: string): {key: string, value: any}[] {
	let returnObject: {key: string, value: any}[] = []
	if (object instanceof Array) {
		if (!parentKey) {
			throw new CustomError('The top-most item cannot be an array.')
		}
		object.forEach((item) => {
			if (item === null) {
				return
			}
			if ((item instanceof Date) || (typeof item !== 'object')) {
				returnObject.push({key: `${parentKey}[]`, value: item})
				return
			}
			returnObject = returnObject.concat(flattenObject(item, `${parentKey}[]`))
			return
		})
		return returnObject
	}
	for (const key in object) {
		const value = object[key]
		if (value === null) {
			continue
		}
		if ((value instanceof Date) || (typeof value !== 'object')) {
			returnObject.push({key: parentKey ? `${parentKey}[${key}]`: key, value})
			continue
		}
		returnObject = returnObject.concat(flattenObject(value, parentKey ? `${parentKey}[${key}]` : key))
	}
	return returnObject
}
