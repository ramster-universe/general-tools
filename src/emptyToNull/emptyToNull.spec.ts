import {emptyToNull} from '../../dist'
import {strict as assert} from 'assert'

describe('emptyToNull', function() {
	it('should execute successfully and return null if the data is undefined', function() {
		const result = emptyToNull(undefined) 
		assert.strictEqual(result, null, `bad value ${result} for the result, expected ${null}`)
	})
	it('should execute successfully and return null if the data is null', function() {
		const result = emptyToNull(null) 
		assert.strictEqual(result, null, `bad value ${result} for the result, expected ${null}`)
	})
	it('should execute successfully and return null if the data is an empty string', function() {
		const result = emptyToNull('') 
		assert.strictEqual(result, null, `bad value ${result} for the result, expected ${null}`)
	})
	it('should execute successfully and return the data as it is if it\'s not undefined, null, an object or a string', function() {
		const result = emptyToNull(1) 
		assert.strictEqual(result, 1, `bad value ${result} for the result, expected ${1}`)
	})
	it('should execute successfully if all parameters are correct and data is an object or an array', function() {
		const result = emptyToNull({
			test1: 'test1string',
			t: ['', {test2: true}, 2],
			a: undefined,
			b: null,
			c: 567,
			d: ''
		}) as {[fieldName: string]: any}
		assert.strictEqual(result.test1, 'test1string', `bad value ${result.test1} for the result.test1, expected ${'test1string'}`)
		assert(result.t instanceof Array, `bad value ${result.t} for the result.t, expected an array`)
		assert.strictEqual(result.t[0], null, `bad value ${result.t[0]} for the result.t[0], expected ${null}`)
		assert.strictEqual(result.t[1].test2, true, `bad value ${result.t[1].test2} for the result.t[1].test2, expected ${true}`)
		assert.strictEqual(result.t[2], 2, `bad value ${result.t[2]} for the result.t[2], expected ${2}`)
		assert.strictEqual(result.a, null, `bad value ${result.a} for the result.a, expected ${null}`)
		assert.strictEqual(result.b, null, `bad value ${result.b} for the result.b, expected ${null}`)
		assert.strictEqual(result.c, 567, `bad value ${result.c} for the result.c, expected ${567}`)
		assert.strictEqual(result.d, null, `bad value ${result.d} for the result.d, expected ${null}`)
	})
})
