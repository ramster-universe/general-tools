/**
 * Takes an object or value and transforms undefined, null and empty strings to null. Recursively does so for objects without mutating the provided data.
 * @param data (required) The object or value to transform.
 * @returns The object or value, with any instances of undefined, null and '' set to null.
 */
export function emptyToNull(data: any): null | string | Object {
	if ((typeof data === 'undefined') || (data === null)) {
		return null
	}
	if (typeof data === 'string') {
		return data.length ? data : null
	}
	if ((typeof data !== 'object') || (data instanceof Date)) {
		return data
	}
	if (data instanceof Array) {
		let outputData = data.map((item) => emptyToNull(item))
		return outputData
	}
	let outputData = {} as any
	for (const key in data) {
		outputData[key] = emptyToNull(data[key])
	}
	return outputData
}
