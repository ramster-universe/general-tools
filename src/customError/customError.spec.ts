import {CustomError} from '../../dist'
import {strict as assert} from 'assert'

describe('checkRoutes', function() {
	it('an error of the CustomError class should have its "name" property to "CustomError" if a message is provided', function() {
		const message = 'An error has occurred.'
		let didThrowAnError = false
		try {
			throw new CustomError(message)
		}
		catch(e) {
			didThrowAnError = true
			assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected ${'CustomError'}`)
			assert.strictEqual(e.message, message, `bad value ${e.message} for e.message, expected ${message}`)
		}
		assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
	})
	it('an error of the CustomError class should have its "name" property to "CustomError" if a message is not provided', function() {
		let didThrowAnError = false
		try {
			throw new CustomError(undefined)
		}
		catch(e) {
			didThrowAnError = true
			assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected ${'CustomError'}`)
			assert.strictEqual(e.message, CustomError.defaultMessage, `bad value ${e.message} for e.message, expected ${CustomError.defaultMessage}`)
		}
		assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected ${true}`)
	})
})
