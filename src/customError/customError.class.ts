export class CustomError extends Error {
	static defaultMessage = 'An internal server error has occurred.'

	constructor(message: string | void) {
		super(message || CustomError.defaultMessage)
		this.name = 'CustomError'
	}
}
