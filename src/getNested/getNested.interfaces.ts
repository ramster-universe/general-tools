export interface IGetNestedOptions {
	arrayItemsShouldBeUnique?: boolean
	removeNestedFieldEscapeSign?: boolean
} 
