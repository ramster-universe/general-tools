import {IArraySortOptions, IArraySortOrderBy, IArraySortSortingDirections} from './arraySort.interfaces'

/**
 * Sorts an array by a list of inner properties. Supports SQL-style sorting by multiple field names and directions.
 * @param array (required) - The array to be sorted.
 * @param orderBy (required) - The ordering options - the fields and directions to sort the array by.
 * @param options (optional) - Additonal ordering options, such as case sensitivity.
 * @returns The sorted array.
 */
export function arraySort (array: any[], orderBy: IArraySortOrderBy[], options?: IArraySortOptions): any[] {
	const caseSensitive: boolean = options ? options.caseSensitive === true : false
	return array.sort((item1, item2) => {
		for (const i in orderBy) {
			const {direction, fieldName} = orderBy[i]
			let a = item1[fieldName],
				aIsDefined = (typeof a !== 'undefined') && (a !== null),
				b = item2[fieldName],
				bIsDefined = (typeof b !== 'undefined') && (b !== null)
			if (!caseSensitive) {
				if (typeof a === 'string') {
					a = a.toLowerCase()
				}
				if (typeof b === 'string') {
					b = b.toLowerCase()
				}
			}
			if (direction === IArraySortSortingDirections.Ascending) {
				if (aIsDefined && !bIsDefined) {
					return 1
				}
				if (!aIsDefined && bIsDefined) {
					return -1
				}
				if (a > b) {
					return 1
				}
				if (a < b) {
					return -1
				}
			} else {
				if (aIsDefined && !bIsDefined) {
					return -1
				}
				if (!aIsDefined && bIsDefined) {
					return 1
				}
				if (a > b) {
					return -1
				}
				if (a < b) {
					return 1
				}
			}
		}
		return 0
	})
}
