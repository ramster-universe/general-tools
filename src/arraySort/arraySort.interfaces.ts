export enum IArraySortSortingDirections {
	Ascending = 'asc',
	Descending = 'desc'
}

export interface IArraySortOrderBy {
	direction: IArraySortSortingDirections
	fieldName: string
}

export interface IArraySortOptions {
	caseSensitive?: boolean
}
