import {arraySort, IArraySortSortingDirections} from '../../dist'
import {strict as assert} from 'assert'

describe('arraySort', function() {
	describe('arraySort -> direction: IArraySortSortingDirections.Ascending', function() {
		it('should execute successfully and sort the array in an ascending order if all parameters are correct, the field to sort by is a number a caseSensitivity option is not provided', function() {
			let sortedArray = arraySort(
				[
					{id: 5},
					{id: 1},
					{id: 3},
					{id: 2},
					{id: 4},
					{id: 7},
					{id: 6}
				],
				[{direction: IArraySortSortingDirections.Ascending, fieldName: 'id'}]
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an ascending order if all parameters are correct, the field to sort by is a string a caseSensitivity option is not provided', function() {
			let sortedArray = arraySort(
				[
					{id: 7, name: 'C'},
					{id: 3, name: 'A'},
					{id: 5, name: 'B'},
					{id: 4, name: 'a'},
					{id: 1},
					{id: 2},
					{id: 6, name: 'b'},
					{id: 9, name: 'd'},
					{id: 8, name: 'c'}
				],
				[{direction: IArraySortSortingDirections.Ascending, fieldName: 'name'}]
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an ascending order if all parameters are correct and the second item (by order of execution) is not defined', function() {
			let sortedArray = arraySort(
				[
					{id: 1},
					{id: 2, name: 'A'}
				],
				[{direction: IArraySortSortingDirections.Ascending, fieldName: 'name'}]
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an ascending order if all parameters are correct, the field to sort by is a string a caseSensitivity option is provided', function() {
			let sortedArray = arraySort(
				[
					{id: 3, name: 'C'},
					{id: 1, name: 'A'},
					{id: 2, name: 'B'},
					{id: 4, name: 'a'},
					{id: 5, name: 'b'},
					{id: 7, name: 'd'},
					{id: 6, name: 'c'}
				],
				[{direction: IArraySortSortingDirections.Ascending, fieldName: 'name'}],
				{caseSensitive: true}
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
	})

	describe('arraySort -> direction: IArraySortSortingDirections.Descending', function() {
		it('should execute successfully and sort the array in an descending order if all parameters are correct, the field to sort by is a number a caseSensitivity option is not provided', function() {
			let sortedArray = arraySort(
				[
					{id: 5, order: 3},
					{id: 1, order: 7},
					{id: 3, order: 5},
					{id: 2, order: 6},
					{id: 4, order: 4},
					{id: 7, order: 1},
					{id: 6, order: 2}
				],
				[{direction: IArraySortSortingDirections.Descending, fieldName: 'id'}]
			)
			for (const i in sortedArray) {
				const orderShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].order, orderShouldBe, `bad value ${sortedArray[i].order} for field "order" at item ${i}, expected ${orderShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an descending order if all parameters are correct, the field to sort by is a string a caseSensitivity option is not provided', function() {
			let sortedArray = arraySort(
				[
					{id: 7, name: 'C', order: 2},
					{id: 3, name: 'A', order: 6},
					{id: 5, name: 'B', order: 4},
					{id: 4, name: 'a', order: 7},
					{id: 6, name: 'b', order: 5},
					{id: 9, name: 'd', order: 1},
					{id: 8, name: 'c', order: 3}
				],
				[{direction: IArraySortSortingDirections.Descending, fieldName: 'name'}]
			)
			for (const i in sortedArray) {
				const orderShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].order, orderShouldBe, `bad value ${sortedArray[i].order} for field "order" at item ${i}, expected ${orderShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an descending order if all parameters are correct and the first item (by order of execution) is not defined', function() {
			let sortedArray = arraySort(
				[
					{id: 1, name: 'A'},
					{id: 2}
				],
				[{direction: IArraySortSortingDirections.Descending, fieldName: 'name'}]
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an descending order if all parameters are correct and the second item (by order of execution) is not defined', function() {
			let sortedArray = arraySort(
				[
					{id: 2},
					{id: 1, name: 'A'}
				],
				[{direction: IArraySortSortingDirections.Descending, fieldName: 'name'}]
			)
			for (const i in sortedArray) {
				const idShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].id, idShouldBe, `bad value ${sortedArray[i].id} for field "id" at item ${i}, expected ${idShouldBe}`)
			}
		})
		it('should execute successfully and sort the array in an descending order if all parameters are correct, the field to sort by is a string a caseSensitivity option is provided', function() {
			let sortedArray = arraySort(
				[
					{id: 3, name: 'C', order: 5},
					{id: 1, name: 'A', order: 7},
					{id: 2, name: 'B', order: 6},
					{id: 4, name: 'a', order: 4},
					{id: 5, name: 'b', order: 3},
					{id: 7, name: 'd', order: 1},
					{id: 6, name: 'c', order: 2}
				],
				[{direction: IArraySortSortingDirections.Descending, fieldName: 'name'}],
				{caseSensitive: true}
			)
			for (const i in sortedArray) {
				const orderShouldBe = parseInt(i, 10) + 1
				assert.strictEqual(sortedArray[i].order, orderShouldBe, `bad value ${sortedArray[i].order} for field "order" at item ${i}, expected ${orderShouldBe}`)
			}
		})
	})
})
