import {
	IFindVertexInGraphGraph,
	IFindVertexInGraphOptions,
	IFindVertexInGraphReturnData,
	// IFindVertexInGraphSearchTypes
} from './findVertexInGraph.interfaces'

/**
 * Finds a vertex in a graph, then returns it and its path.
 * @param graph (required) The graph to search in.
 * @param vertexId (required) The id of the vertex to search for.
 * @param options (optional) Method options, such as the search type (currently DFS only).
 * @returns An object in the format {pathToVertex: string | null, vertex: IFindVertexInGraphGraphVertex | null}.
 */
export function findVertexInGraph(
	graph: IFindVertexInGraphGraph,
	vertexId: string,
	options: IFindVertexInGraphOptions
): IFindVertexInGraphReturnData {
	const currentVertexPath = options.currentVertexPath ? `${options.currentVertexPath}.` : '',
		vertex = graph[vertexId]
	if (typeof vertex === 'undefined') {
		// const {searchType} = options
		// if (searchType === 'BFS') {
		// 	for (const childVertexId in graph) {
		// 		const child = graph[childVertexId],
		// 			children = child.children
		// 		if (!children) {
		// 			continue
		// 		}
		// 		if (typeof children[vertexId] === 'undefined') {
		// 			for (const innerChildVertexId in children) {
		// 			}
		// 			// return {pathToVertex: null, vertex: null}
		// 		}
		// 		return {pathToVertex: currentVertexPath, vertex: children[vertexId]}
		// 	}
		// }
		// if (searchType === IFindVertexInGraphSearchTypes.DFS) {
			for (const childVertexId in graph) {
				const child = graph[childVertexId]
				if (!child.children) {
					continue
				}
				const result = findVertexInGraph(
					child.children,
					vertexId,
					{...options, currentVertexPath: `${currentVertexPath}${childVertexId}.children`}
				)
				if (result.pathToVertex) {
					return result
				}
			}
			// return {pathToVertex: null, vertex: null}
		// }
		return {pathToVertex: null, vertex: null}
	}
	return {
		pathToVertex: `${currentVertexPath}${vertexId}`,
		vertex
	}
}
