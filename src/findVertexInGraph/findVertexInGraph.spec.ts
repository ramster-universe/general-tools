import {findVertexInGraph, IFindVertexInGraphSearchTypes} from '../../dist'
import {strict as assert} from 'assert'

describe('findVertexInGraph', function() {
	describe('findVertexInGraph -> searchType = "DFS"', function() {
		it('should execute successfully and return an IFindVertexInGraphReturnData object with null values if the vertex does not exist in the provided graph', function() {
			let result = findVertexInGraph(
				{0: {children: {10: {data: 'test'}}}},
				'1',
				{searchType: IFindVertexInGraphSearchTypes.DFS}
			)
			assert.strictEqual(result.pathToVertex, null, `bad value ${result.pathToVertex} for result.pathToVertex, expected ${null}`)
			assert.strictEqual(result.vertex, null, `bad value ${result.vertex} for result.vertex, expected ${null}`)
		})
		it('should execute successfully and return an IFindVertexInGraphReturnData object with the correct path and vertex object values if the vertex does exist in the provided graph and it exists at the top level', function() {
			let result = findVertexInGraph(
				{0: {children: {10: {data: 'test'}}, data: 't0'}},
				'0',
				{searchType: IFindVertexInGraphSearchTypes.DFS}
			)
			assert.strictEqual(result.pathToVertex, '0', `bad value ${result.pathToVertex} for result.pathToVertex, expected ${'0'}`)
			assert.notStrictEqual(result.vertex, null, `bad value ${result.vertex} for result.vertex, expected not ${null}`)
			// this if will always be true, but we need because the TypeScript compilator complains that result.vertex might be null
			if (result.vertex) {
				assert.strictEqual(result.vertex.data, 't0', `bad value ${result.vertex.data} for result.vertex.data, expected ${'t0'}`)
			}
		})
		it('should execute successfully and return an IFindVertexInGraphReturnData object with the correct path and vertex object values if the vertex does exist in the provided graph and it exists in first-level depth', function() {
			let result = findVertexInGraph(
				{0: {children: {10: {data: 'test'}}, data: 't0'}},
				'10',
				{searchType: IFindVertexInGraphSearchTypes.DFS}
			)
			assert.strictEqual(result.pathToVertex, '0.children.10', `bad value ${result.pathToVertex} for result.pathToVertex, expected ${'0.children.10'}`)
			assert.notStrictEqual(result.vertex, null, `bad value ${result.vertex} for result.vertex, expected not ${null}`)
			// this if will always be true, but we need because the TypeScript compilator complains that result.vertex might be null
			if (result.vertex) {
				assert.strictEqual(result.vertex.data, 'test', `bad value ${result.vertex.data} for result.vertex.data, expected ${'test'}`)
			}
		})
		it('should execute successfully and return an IFindVertexInGraphReturnData object with the correct path and vertex object values if the vertex does exist in the provided graph and it exists in n-th level depth', function() {
			let result = findVertexInGraph(
				{0: {children: {10: {children: {15: {data: 't1'}}, data: 'test'}}, data: 't0'}},
				'15',
				{searchType: IFindVertexInGraphSearchTypes.DFS}
			)
			assert.strictEqual(result.pathToVertex, '0.children.10.children.15', `bad value ${result.pathToVertex} for result.pathToVertex, expected ${'0.children.10.children.15'}`)
			assert.notStrictEqual(result.vertex, null, `bad value ${result.vertex} for result.vertex, expected not ${null}`)
			// this if will always be true, but we need because the TypeScript compilator complains that result.vertex might be null
			if (result.vertex) {
				assert.strictEqual(result.vertex.data, 't1', `bad value ${result.vertex.data} for result.vertex.data, expected ${'t1'}`)
			}
		})
	})
})
