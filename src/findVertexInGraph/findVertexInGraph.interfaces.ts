export interface IFindVertexInGraphGraph {
	[vertexId: string]: IFindVertexInGraphGraphVertex
}

export interface IFindVertexInGraphGraphVertex {
	children?: IFindVertexInGraphGraph
	data?: any
}

export enum IFindVertexInGraphSearchTypes {
	// BFS = 'BFS',
	DFS = 'DFS'
}

export interface IFindVertexInGraphOptions {
	currentVertexPath?: string
	searchType: IFindVertexInGraphSearchTypes
}

export interface IFindVertexInGraphReturnData {
	pathToVertex: string | null
	vertex: IFindVertexInGraphGraphVertex | null
}
