/**
 * Takes a nested object and turns its non-Date, non-null object keys into stringified _json_ keys, which can later be decoded by the decodeQueryValues method.
 * @param data (required) - The object to be processed.
 * @returns The processed object.
 */
export function stringifyNestedObjects(data: {[key: string]: any}): {[key: string]: any} {
	let stringifiedObject: {[fieldName: string]: any} = {}
	for (const key in data) {
		const value = data[key]
		if ((value instanceof Array) || ((typeof value === 'object') && (value !== null) && !(value instanceof Date))) {
			stringifiedObject[`_json_${key}`] = JSON.stringify(value)
			continue
		}
		stringifiedObject[key] = value
	}
	return stringifiedObject
}
