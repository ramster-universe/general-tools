import {stringifyNestedObjects} from '../../dist'
import {strict as assert} from 'assert'

describe('stringifyNestedObjects', function() {
	it('should execute successfully and stringify the provided non-date object keys', function() {
		const date = new Date(),
			sourceObject = {
				testKey: [{testKey1: 'test'}, date],
				testKey2: 'test test test',
				testKey3: {testKey4: [null, 'test', {tq: 11}]}
			},
			result = stringifyNestedObjects(sourceObject),
			resultShouldBe = {
				_json_testKey: JSON.stringify(sourceObject.testKey),
				testKey2: sourceObject.testKey2,
				_json_testKey3: JSON.stringify(sourceObject.testKey3)
			} as any
		for (const key in resultShouldBe) {
			assert.strictEqual(
				result[key],
				resultShouldBe[key],
				`bad value ${result[key]} for result[key] at key "${key}", expected ${resultShouldBe[key]}`
			)
		}
	})
})
